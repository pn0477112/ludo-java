package horse;


import horse.ImgPath.Color;
import horse.ImgPath.Theme;
import java.util.List;



public class Horse {
    
    private static GameState game;
    private static Chongiaodien selectWindow;
    
    public static void main(String[] args) {     
    	
        new ImgPath();

    	selectWindow= new Chongiaodien("theme");
    	Theme theme = selectWindow.selectedTheme();
        
    	selectWindow= new Chongiaodien("player", theme.name());
    	List<Color> plColors = selectWindow.selectedPlayers();
        
    	selectWindow= new Chongiaodien("board", theme.name());
    	boolean special = selectWindow.selectedBoard();

    	game = new GameState(theme, plColors, special);
        
        HorseGUI.drawGUI(game);
        System.exit(0);
    }
}
