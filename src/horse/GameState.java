package horse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import horse.Tudongchoi.AutoplayMode;     
import horse.ImgPath.*;
import static horse.Player.GOAL;
import static horse.Player.OUT_OF_BOARD;

/**
 * Đại diện cho trạng thái của từng phần tử có trong một vòng Ludo.
 * Thông tin trạng thái này bao gồm:
 * <ul>
 * <li> MapGame nơi trò chơi được chơi
 <li> Con xúc xắc
 * <li> Tập hợp các trình phát
 * <li> Bộ điều khiển cho trình phát máy tính (Tudongchoi)
 <li> Chủ đề cho biểu diễn đồ họa
 * <li> Chỉ số của người chơi hiện tại tại bất kỳ thời điểm nào
 * <li> Nếu một hiệp Ludo đang được chơi
 * <li> Nếu người chơi hiện tại nên tung xúc xắc hoặc di chuyển mã thông báo
 * <li> Danh sách chỉ số của những người chơi tích cực trong vòng đấu
 * <li> Danh sách các nước đi có thể có tại bất kỳ thời điểm nào
 * <li> Danh sách những người chơi đã hoàn thành trò chơi (tất cả các thẻ ở khu vực khung thành)
 * <li> Kết quả của một hiệp đấu
 * </ul>
 * <p>
 */

public class GameState {
    
    //default game settings:
    public static final Theme DEFAULT_THEME = Theme.plain;
    public static final boolean DEFAULT_BOARD = true;
    public static final boolean DEFAULT_AUTOPLAYER= false;
    public static final AutoplayMode DEFAULT_AUTOMODE= AutoplayMode.customAI;
    private static List<ImgPath.Color> DEFAULT_PLAYERS = Arrays.asList(Color.blue, Color.red, Color.yellow, Color.green);
    
    private MapGame board;
    private Dice dice;
    private Player[] players;
    private Tudongchoi computerPlayer;
    private Theme theme;
    private int currentPlayer, turn; //chỉ số của người chơi hiện tại, số lượt kể từ trận đấu

    private boolean diceRoller, playing, debug; // kiểm tra xem nhấp chuột là con lăn xúc xắc hay bộ chọn mã thông báo
    private ArrayList<Integer> xPlayers, xTokens, winners; //chỉ số của những người chơi đang hoạt động
    private String gameResults;
    /** 
    * Khởi tạo một phiên bản của GameState bằng cài đặt trò chơi mặc định. 
    */
    public GameState(){
        this.gameResults = "";
        initVars();
        this.theme = DEFAULT_THEME;
        this.board = new MapGame(DEFAULT_BOARD);
        createSetOfPlayers(DEFAULT_PLAYERS, DEFAULT_AUTOPLAYER, DEFAULT_AUTOMODE);
    } 
    
    /**
    * Khởi tạo một phiên bản của GameState chỉ định chủ đề, danh sách người chơi và loại bảng.
    * Sử dụng các cài đặt mặc định cho cài đặt tự động / thủ công và AutoplayMode.
    *
    * @param theme chủ đề hoặc phong cách cho GUI
    * @param plcolor màu sắc của các con cờ trong vòng này
    * @param specialBoard cho dù bảng là thông thường (sai) hay đặc biệt (đúng)
    *
    */
    public GameState(Theme theme, List<ImgPath.Color> plColors, boolean specialBoard){
        this.gameResults = "";
        initVars();
        this.theme = theme;
        this.board = new MapGame(specialBoard);
        createSetOfPlayers(plColors, DEFAULT_AUTOPLAYER, DEFAULT_AUTOMODE);
    } 
    
    /**
    * Khởi tạo một phiên bản của GameState chỉ định cài đặt tự động / thủ công và AutoplayMode của mỗi người chơi.
    * Nó cũng chỉ định chủ đề, danh sách người chơi và loại bảng.
    * Hàm tạo này không sử dụng cài đặt mặc định.
    *
    * @param theme đề chủ đề hoặc phong cách cho GUI
    * @param plcolor Màu danh sách màu sắc của các cầu thủ trong vòng này
    * @param specialBoard cho dù bảng là thông thường (sai) hay đặc biệt (đúng)
    *@param auto nếu người chơi là người chơi máy tính (đúng) hoặc người chơi là người (sai)
    * @param autoMode thuật toán máy tính được trình phát máy tính sử dụng để quyết định nước đi tiếp theo
    */
    public GameState(Theme theme, List<ImgPath.Color> plColors, List<Boolean> auto, List<AutoplayMode> autoMode, boolean specialBoard){
        initVars();
        this.theme = theme;
        this.board = new MapGame(specialBoard);
        createSetOfPlayers(plColors, auto, autoMode);
    } 
    
    /**
* Khởi tạo các biến chung cho tất cả các hàm tạo GameState.
    */
    private void initVars(){
        this.dice = new Dice();
        this.playing = true;
        this.currentPlayer = 0;
        this.turn = 0;
        this.xPlayers=new ArrayList<>(); 
        this.players = new Player[4];
        this.diceRoller=true;
        this.xTokens=new ArrayList<>();
        this.winners=new ArrayList<>();
        this.computerPlayer= new Tudongchoi();
        this.gameResults = "";
        this.debug = false;
    }

    /**
    * Nhận trạng thái hiện tại của đối tượng đại diện cho một bảng ludo.
    * Lớp MapGame chứa thông tin về loại bảng (thông thường hoặc đặc biệt),
 vị trí của các ô đặc biệt và hình ảnh bảng cho GUI
    * @return trạng thái hiện tại của bảng
 
    */
    public MapGame getBoard() {
        return this.board;
    }

    /**
    * Đặt bảng thuộc tính cục bộ thành giá trị của bảng tham số.
     */
    public void setBoard(MapGame board) {
        this.board = board;
    }

     /**
     Đặt thuộc tính đặc biệt từ bảng đối tượng thành thông thường (sai) hoặc đặc biệt (đúng).
     * @param boolean đặc biệt cho biết bảng có nên đặc biệt hay không
     */
    public void setSpecial(String special) {
        if(special.equals("special"))
            this.board.setSpecial(true);
    }

    /**
     * Nhận trạng thái hiện tại của đối tượng đại diện cho một con xúc xắc
     * Lớp Xúc xắc chứa thông tin về kết quả sau khi đổ xúc xắc,
     * giá đỡ xúc xắc hiện tại và hình ảnh xúc xắc cho GUI
     * @return trạng thái hiện tại của xúc xắc

     */
    public Dice getDice() {
        return this.dice;
    }

    /**
    * Đặt xúc xắc thuộc tính cục bộ thành giá trị của xúc xắc tham số
     * @param xúc xắc một phiên bản của xúc xắc đẳng cấp

     */
    public void setDice(Dice dice) {
        this.dice = dice;
    }

    /**
    * Nhận một mảng với các đối tượng của Người chơi trong lớp, đại diện cho từng người chơi trong vòng này
     * @ quay lại nhóm người chơi cho vòng này
     */
    public Player[] getPlayers() {
        return this.players;
    }

    /**
     * Đặt các trình phát thuộc tính để tham chiếu các trình phát mảng tham số
     * Người chơi @param mảng kích thước 4 chứa các đối tượng của trình phát lớp
     */
    public void setPlayers(Player[] players) {
        this.players = players;
    }
    
    /**
     Trả về một đối tượng của lớp Người chơi đã cho chỉ số i của nó trong mảng người chơi thuộc tính
     * @param i số nguyên [0-3] đại diện cho chỉ số của một trong 4 người chơi
     * @return player của chỉ số i
     */
    public Player getPlayer(int i) {
        return this.players[i];
    }
    
    /**
     * Trả về một đối tượng của lớp Player với màu sắc của nó
     * Chuỗi màu @param chứa tên thuộc tính màu của người chơi
     * @return player của màu Color
     */
    public Player getPlayer(String color) {
        for (Player player: this.players)
            if(player.getColor().equalsIgnoreCase(color))
                return player;
        return null;
    }
    
    /**
     * Nhận danh sách chỉ số của những người chơi đang hoạt động
     * (Những người chơi đã bắt đầu một vòng đấu nhưng chưa có 4 thẻ trong Khu vực mục tiêu).
     * @ quay lại danh sách chỉ số của những người chơi tích cực trong vòng đấu
     */
    public ArrayList<Integer> getXPlayers() {
        return this.xPlayers;
    }

    /**
     Thêm chỉ số người chơi vào danh sách người chơi đang hoạt động
     * Sau khi thêm người chơi vào danh sách, hãy sắp xếp danh sách theo thứ tự tăng dần để giữ thứ tự lượt của mỗi người chơi
     * @param playerIndex integer number [0-3] đại diện cho chỉ số của một trong 4 người chơi sẽ được thêm vào
     */
    public void addXPlayers(int playerIndex) {
        this.xPlayers.add(playerIndex);
        Collections.sort(this.xPlayers);
    }
    
    /**
    * Xóa chỉ số người chơi khỏi danh sách người chơi đang hoạt động
     * @param playerIndex integer number [0-3] đại diện cho chỉ số của một trong 4 người chơi sẽ bị xóa */
    public void removeXPlayers(int playerIndex) {
        this.xPlayers.remove(this.xPlayers.indexOf(playerIndex));
    }
    
    /**
    * Nhận danh sách chỉ mục của các mã thông báo đang hoạt động của người chơi hiện tại
     * (Mã thông báo có thể được di chuyển vào bất kỳ thời điểm nào).
     * @return danh sách các động thái có thể*/
    public ArrayList<Integer> getXTokens() {
        return this.xTokens;
    }

    /**
     * Gets the list of players that completed the game (have all their tokens at the goal area)
     * @return list of winners
     */
    public ArrayList<Integer> getWinners() {
        return this.winners;
    }

    /**
     * Adds a player to the list of winners
     * @param playerIndex integer number [0-3] representing the index of one of the 4 players
     */
    public void addWinners(int playerIndex) {
        this.winners.add(playerIndex);
    }

     /**
     * Gets the current theme/style used for the GUI
     * @return the theme/style of the game graphics
     */
    public Theme getTheme() {
        return this.theme;
    }

    /**
     * Sets the game graphics style to the given theme
     * @param theme graphic style for the GUI
     */
    public void setTheme(Theme theme) {
        this.theme = theme;
    }
    
    /**
     * Sets the game graphics style to the given theme name
     * @param theme string with the name of the theme/graphic style for the GUI
     */
    public void setTheme(String theme) {
        for(Theme t: Theme.values())
            if(theme.equalsIgnoreCase(t.name()))
                this.theme = t;
    }

    /**
     * Indicates whether it is time for the current player to roll the dice (true) or to select and move a token (false).
     * @return true if the current player should roll the dice, false if the player should move a token
     */
    public boolean getDiceRoller() {
        return this.diceRoller;
    }
    
    /**
     * Indicates whether a round of ludo is being played (true) or not (false).
     * @return true if a game is being played, false if the game is over
     */
    public boolean getPlaying() {
        return this.playing;
    }
    
    /**
     * Sets the value of the attribute playing to the given parameter playing
     * @param playing boolean value indicating if the round of ludo should be started (true) or terminated(false)
     */
    public void setPlaying(boolean playing) {
        this.playing=playing;
    }
    
        /**
     * Indicates whether a test is being performed.
     * @return true if the game is in test mode
     */
    public boolean getDebug() {
        return this.debug;
    }
    
    /**
     * Sets the value of the attribute debug to the given parameter debug
     * @param debug boolean value indicating if the game is in debug mode (a test is being performed)
     */
    public void setDebug(boolean debug) {
        this.debug=debug;
    }

    /**
     * Gets the index of the current player in the array players
     * @return integer number [0-3] representing the index of current player
     */
    public int getCurrentPlayer(){
        return this.currentPlayer;
    }
    
     /**
     * Sets the current player to a given index in the range 0-3
     */
    public void setCurrentPlayer(int index){
        this.currentPlayer=index%4;
    }
    
    /**
     * Gets the number of played turns at any given moment
     * A turn starts when a player gets the dice to roll and ends when the dice is passed to the next player
     * @return total number of played turns
     */
    public int getTurnCount(){
        return this.turn;
    }
    
    /**
     * Gets a string with the results of a round of ludo
     * This is a list of winners by first to last to have finished the game
     * @return string containing the results of the game
     */
    public String getGameResults(){
    	return this.gameResults;
    }

    /**
     * Initializes the attributes of each of the active players for this round.
     * @param colors list of colors of the active players for this round
     * @param auto list of boolean values for each player. the boolean value for each player should be 
     *             true if the player is computer operated, and false if the player is human
     * @param autoMode list of AutoplayMode values for each player. This mode will be used if/when auto is set to true
     */
    private void createSetOfPlayers(List<ImgPath.Color> colors, List<Boolean> auto, List<AutoplayMode> autoMode) {
        int i = 0;
        for (ImgPath.Color c : ImgPath.Color.values()) {
            this.players[i] = new Player(c, auto.get(i), autoMode.get(i));
            this.players[i].setPIndex(i);
            //System.out.println("Player "+c.name()+" created.");
            for(int j=0; j<4; j++){
                this.players[i].setXY(j);
            }
            if (colors.contains(c)) {
                this.players[i].setActive(true);
                this.xPlayers.add(i);
            }
            i++;
        }
    }
   
    /**
     * Initializes the attributes of each of the active players for this round.
     * In this case the auto and AutoplayMode settings are the same for all players
     * @param colors list of colors of the active players for this round
     * @param auto true if the players are computer operated, and false if the players are human
     * @param autoMode AutoplayMode value for all players. This mode will be used if/when auto is set to true
     */
    private void createSetOfPlayers(List<ImgPath.Color> colors, boolean auto, AutoplayMode autoMode) {
        int i = 0;
        for (ImgPath.Color c : ImgPath.Color.values()) {
            this.players[i] = new Player(c, auto, autoMode);
            this.players[i].setPIndex(i);
            //System.out.println("Player "+c.name()+" created.");
            for(int j=0; j<4; j++){
                this.players[i].setXY(j);
            }
            if (colors.contains(c)) {
                this.players[i].setActive(true);
                this.xPlayers.add(i);
            }
            i++;
        }
    }
    
    /**
     * Sends all the tokens of each active player to its starting position
     * and sets attributes turn, currentPlater and diceRoller to its original values.
     */
    public void restart(){
        for(Player p: players)
            if(p.getActive())
                p.reset();
        turn = 0;
        currentPlayer = 0;
        diceRoller=true;
    }
    
    /**
     * Initializes a player that was previously considered not-active in the game
     * @param color string containing the name of the player's color attribute
     */
    public void addPlayer(String color){
        if(!this.getPlayer(color).getActive()){
            this.getPlayer(color).reset();
            this.getPlayer(color).setActive(true);
            addXPlayers(this.getPlayer(color).getPIndex());
            turn = this.xPlayers.indexOf(currentPlayer);}
    }
    
    /**
     * Removes an active player from the game given its color
     * @param color string containing the name of the player's color attribute
     */
    public void removePlayer(String color){
        this.getPlayer(color).setActive(false);
        removeXPlayers(this.getPlayer(color).getPIndex());
        turn = this.xPlayers.indexOf(currentPlayer);
    }    

     /**
     * Compares the position of a given token with other players' tokens and if equal, sends the opponent's token to its home area
     * @param pIndex integer number [0-3] representing the index of the given player
     * @param tIndex integer number [0-3] representing the index of the given token
     */
    private void checkOtherTokens(int pIndex, int tIndex) {
        int tokenPosition = this.players[pIndex].getToken(tIndex).getPosition();
        for (int i = 0; i < this.xPlayers.size(); i++) {
            if (!this.players[pIndex].getColor().equals(players[xPlayers.get(i)].getColor())) {
                for (int j = 0; j < 4; j++) {
                    if (players[xPlayers.get(i)].getToken(j).getPosition() == tokenPosition && players[xPlayers.get(i)].getToken(j).getPosition() != OUT_OF_BOARD && !players[xPlayers.get(i)].getToken(j).getSafe()) {
                        players[xPlayers.get(i)].outOfBoard(j);
                    }
                }
            }
        }
    }

    /**
     * The current player rolls the dice and updates the list of active tokens
     */
    public void rollAndCheckActiveTokens() {
        this.currentPlayer = this.xPlayers.get(this.turn % this.xPlayers.size());
        this.dice.rollDice(this.currentPlayer);
        //System.out.printf("%s player rolls the dice: %d\n", this.players[currentPlayer].getColor(), this.dice.getResult());
        this.xTokens.clear();

        if (this.dice.getIsSix()) {
            this.players[currentPlayer].setTurn(true);// flag for throwing the dice again if a token is moved
            for (Mathongbao token : this.players[currentPlayer].getTokens()) {
                if (!(token.getFinalTrack() && token.getPosition() == GOAL)) {
                    this.xTokens.add(token.getIndex());
                }
            }
        } else {
            this.players[currentPlayer].setTurn(false);
            for (int index : this.players[currentPlayer].getTokensOut()) {
                this.xTokens.add(index);
            }
        }
    }
    
    /**
     * If there are no possible moves, the current player passes the dice to the next player 
     */
    public void checkMoveOrPass(){
        if (this.xTokens.size() > 0) {
            this.diceRoller = false;} 
        else { //if no tokens to move, pass and let player roll dice
            this.turn++;
            //System.out.println("next turn player " + this.players[currentPlayer].getColor());
        }
        this.currentPlayer = this.xPlayers.get(this.turn % this.xPlayers.size());
    }
    
    /**
     * Moves the selected token to a new position, checks for other tokens and special tiles (in case of special board), 
     * checks if the player has finished the game in this turn, and if so, checks if the game is over
     * @param tokenIndex integer number [0-3] representing the index of the selected token
     */
    public void selectAndMove(int tokenIndex) {
        Mathongbao thisToken = this.players[currentPlayer].getTokens()[tokenIndex];
        //System.out.println((thisToken.getFinalTrack()&&!thisToken.getOut()));
        if(!(thisToken.getFinalTrack()&&!thisToken.getOut())){
        this.players[currentPlayer].moveSelectedToken(tokenIndex,this.dice.getResult());
        if (!thisToken.getFinalTrack()) {
            this.checkOtherTokens(this.players[currentPlayer].getPIndex(), tokenIndex);
            if (this.board.getSpecial()) {
                this.players[currentPlayer].checkSpecial(tokenIndex, this.board);
            }
        }
        if (this.players[currentPlayer].getGoal() == 4) {
            this.addWinners(this.players[currentPlayer].getPIndex());
            this.removeXPlayers(this.players[currentPlayer].getPIndex());
            if (this.getXPlayers().isEmpty()) {
                this.playing = false;
                this.gameResults = "\nResults:\n\n";
                //System.out.println("-----GAME OVER!-----"+gameResults);
                for (int i = 0; i < this.getWinners().size(); i++) {
                    //System.out.printf("%d place - %s player\n", i + 1, this.getPlayers()[this.getWinners().get(i)].getColor());
                    this.gameResults += (i + 1)+" place - "+this.getPlayers()[this.getWinners().get(i)].getColor()+" player\n";
                }
            }
        }
        //System.out.println("Player turn:" + this.players[currentPlayer].getTurn());
        if (!this.players[currentPlayer].getTurn()) {
            this.turn++;
            //System.out.println("next turn player " + this.players[currentPlayer].getColor());
        }
        this.diceRoller = true;
        if (playing)
            this.currentPlayer = this.xPlayers.get(this.turn % this.xPlayers.size());
        }
    }
     
    /**
     * Calls the method SelectAndMove with the token selected by the computer player as the argument
     * @see #selectAndMove(int)
     */
    public void autoMove() {
        selectAndMove(computerPlayer.selectToken(this));
    }
}
