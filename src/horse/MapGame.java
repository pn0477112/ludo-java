package horse;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import horse.ImgPath.Theme;


public class MapGame {

    public static final List<Integer> START_TILES = Arrays.asList(0, 13, 26, 39);
    public static final List<Integer> GLOBE_TILES = Arrays.asList(8, 21, 34, 47);
    public static final List<Integer> STAR_TILES = Arrays.asList(5, 11, 18, 24, 31, 37, 44, 50);

    private boolean special;
    private final Map<Theme, BufferedImage> img = new HashMap<>();
    private final Map<Theme, BufferedImage> imgsp = new HashMap<>();

    /**
    * Khởi tạo một phiên bản của Board theo tham số đặc biệt
     * @param special true nếu bảng phải chứa các ô đặc biệt (ngôi sao và quả địa cầu) hoặc false nếu không

     */
    public MapGame(boolean special) {
        this.special = special;
        for (Theme t : Theme.values()) {
            ImgPath.setBoardPath(t);
            try {
                img.put(t, ImageIO.read(new File(ImgPath.getBoardPath(ImgPath.Board.board))));
                imgsp.put(t, ImageIO.read(new File(ImgPath.getBoardPath(ImgPath.Board.specialboard))));
            } catch (IOException ex) {
                System.out.println("Image not found.");
            }
        }
    }

    /**
     * Nhận biểu diễn đồ họa của bảng
     * @param chủ đề chủ đề / phong cách đồ họa của trò chơi
     * @ quay lại hình ảnh được sử dụng để đại diện cho bảng
     * /
     */
    public BufferedImage getImg(Theme theme) {
        return img.get(theme);
    }

    /**
     * Gets a mask for the board containing the graphics for the special tiles
     * @param theme the theme/graphic style of the game
     * @return the image mask used to mark the location of the special tiles (stars and globes) in the board
     */
    public BufferedImage getImgSp(Theme theme) {
        return imgsp.get(theme);
    }

    /**
     * Indicates whether the board includes special tiles (stars and globes)
     * @return true if the board includes special tiles, false otherwise
     */
    public boolean getSpecial() {
        return this.special;
    }

    /**
     * Sets the board layout to regular/special given the boolean special
     * @param special true if the board should include special tiles (stars and globes) and false if not
     */
    public void setSpecial(boolean special) {
        this.special = special;
    }

    /**
     * Sets the board layout to regular/special given the string special
     * @param special if the string equals "special" ignoring case, the board should include special tiles
     */
    public void setSpecial(String special) {
        this.special = special.equalsIgnoreCase("special");
    }
}
