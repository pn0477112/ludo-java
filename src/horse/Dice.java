package horse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;
import javax.imageio.ImageIO;
import static javax.swing.JOptionPane.showInputDialog;
import static horse.ImgPath.DICE_PATH;
import static horse.ImgPath.FILE_EXTENSION;
import static horse.HorseGUI.TILE_SIZE;

/**
 * Đại diện cho một viên xúc xắc thông thường với 6 kết quả có thể xảy ra.
 * Đối tượng này chứa các thông tin sau:
 * <ul>
 * <li> Kết quả thu được sau khi đổ xúc xắc
 * <li> Cho dù kết quả xúc xắc đang được tạo ngẫu nhiên hay do người dùng nhập
 * <li> Người chơi hiện đang cầm xúc xắc
 * <li> Số lần xúc xắc được tung vào bất kỳ thời điểm nào
 * <li> Hình ảnh đại diện cho đối tượng này trong GUI
 * </ul>
 * <p>
 *
 * @author Carla Villegas <carv@itu.dk>
 */
public class Dice{

    public static final int DICE_SIZE=(int)(1.5*TILE_SIZE);
    private final Random random = new Random();
    private Scanner scanner;
    private final int ACC=0;
    private int result = 3;
    private int pIndex; //player holding the dice
    private boolean isSix = false,
                    animated = false,
                    debug = false;
    private int[] coordinates = new int[2];
    private int tickCounter;
    private int pos, vel;
    private int diceRollCount;
    
    private BufferedImage dice[] = new BufferedImage[6];
    private BufferedImage diceAnimation[] = new BufferedImage[25];
    private BufferedImage diceImg;
  
    /**
     *  Khởi tạo một phiên bản của Dice
     */
        public Dice(){  
        try {
            for (int i=0; i<dice.length; i++) 
                    dice[i]=ImageIO.read(new File(DICE_PATH+"result"+(i+1)+FILE_EXTENSION));
            for (int i=0; i<diceAnimation.length; i++) 
                    diceAnimation[i]=ImageIO.read(new File(DICE_PATH+"animateddice"+(i+1)+FILE_EXTENSION));}
        catch (IOException ex) {
                System.out.println("Image not found.");}
        diceImg = dice[result-1];
        this.coordinates[0]=(TILE_SIZE*15-DICE_SIZE)/2;
        this.coordinates[1]=(TILE_SIZE*15-DICE_SIZE)/2;
    }
    
    /**
     * Nhận số nguyên trong phạm vi [1-6] có được sau khi đổ xúc xắc.
     * @return kết quả sau khi tung xúc xắc trong phạm vi [1-6].
     */
    public int getResult() {
        return this.result;
    }
    
    /**
     * Đặt kết quả xúc xắc thành một số nhất định
     */
    public void setResult(int result) {
        this.result = result;
        this.isSix = this.result == 6;
    }
    
    /**
     * Đại diện cho số lần xúc xắc được tung ra trong một trò chơi.
     * @return số lượng tổng số lần đổ xúc xắc
     */
    public int getDiceRollCount() {
        return this.diceRollCount;
    }

    /**
    * Nhận kết quả xúc xắc mới bởi cùng một người chơi/người giữ.     */
    public void reRoll() {
        this.result = this.roll();
        this.isSix = this.result == 6;
    }

    /**
     * Nhận chỉ số của người chơi hiện đang cầm xúc xắc.
     * @return int number [0-3] đại diện cho chỉ số của người chơi cầm xúc xắc

     */
    public int getHolder() {
        return this.pIndex;
    }

    /**
     * Đưa xúc xắc cho người chơi có chỉ số cho trước.
     * @param pIndex int number [0-3] đại diện cho chỉ số của người chơi nhận xúc xắc
     */
    public void setHolder(int pIndex) {
        this.pIndex = pIndex;
    }

    /**
     *Cho biết kết quả xúc xắc có là sáu hay không.
     * @ trả về true nếu kết quả xúc xắc là sáu, ngược lại là false
     */
    public boolean getIsSix() {
        return this.isSix;
    }
    
    /**
     * Mô phỏng rằng kết quả xúc xắc bằng sáu hoặc không bằng sáu.
     * Phương pháp này được sử dụng cho mục đích kiểm tra / gỡ lỗi.
     * @param isSix true nếu kết quả xúc xắc mô phỏng số sáu, false nếu nó mô phỏng bất kỳ kết quả nào khác
     */
    public void setIsSix(boolean isSix) {
        this.isSix = isSix;
    }
    
    /**
    * Cho biết kết quả xúc xắc được tạo ngẫu nhiên hay do người dùng nhập.
     * @return true nếu kết quả xúc xắc được tạo bởi người dùng nhập, false nếu kết quả ngẫu nhiên
     */
    public boolean getDebug() {
        return this.debug;
    }
    
    /**
   * Bật / tắt chế độ gỡ lỗi.
     * @param gỡ lỗi true cho kết quả xúc xắc do người dùng tạo, false cho kết quả ngẫu nhiên
     */
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    /**
     * Lấy tọa độ của xúc xắc trong khung GUI.
     * @param i 0 để truy xuất tọa độ x, 1 để truy xuất tọa độ y
     * @return vị trí của xúc xắc theo pixel (tọa độ x hoặc y) trong khung GUI
  
     */
    public int getCoordinates(int i) {
        return this.coordinates[i];
    }
    
     /**
    * Nhận biểu diễn đồ họa của xúc xắc tại bất kỳ thời điểm nào.
     * @return hình ảnh được sử dụng để đại diện cho xúc xắc trong GUI

     */
    public BufferedImage getDiceImg() {
        return this.diceImg;
    }
    
    /**
    * Người chơi có chỉ số pIndex tung xúc xắc.
     * @param pIndex number [0-3] đại diện cho chỉ số của người chơi sẽ ném xúc xắc

     */
    public void rollDice(int pIndex) {
        this.pIndex = pIndex;
        this.result = this.roll();
        this.isSix = this.result == 6;
        resetCoordinates();
        this.tickCounter=0;
        this.vel=1;
        this.diceRollCount++;
    }

    /**
     Tạo kết quả xúc xắc.
     * Trong chế độ gỡ lỗi, kết quả được tạo bởi đầu vào của người dùng, nếu không, kết quả được tạo ngẫu nhiên.
     * @ quay lại kết quả hoặc một lần tung xúc xắc trong phạm vi [1-6].
     */
    public int roll() {
        if(!debug)
            this.result = random.nextInt(6) + 1;
        else{
            scanner = new Scanner(showInputDialog("Enter dice value (1-6):"));
            try{int res = scanner.nextInt()%7;
                this.result = res!=0? res:6;
            }
            catch(NoSuchElementException ne){this.result=6;}     
            }
        return this.result;
    }
    
    /**
     * Đặt tọa độ xúc xắc vào góc của người chơi cầm xúc xắc.
*/
    public void resetCoordinates(){
        pos=0;
        if(this.pIndex<2){
            this.coordinates[0]=TILE_SIZE*15-DICE_SIZE;}
        else{
            this.coordinates[0]=0;}
        if(this.pIndex%3==0){
            this.coordinates[1]=0;}
        else{
            this.coordinates[1]=TILE_SIZE*15-DICE_SIZE;}   
    }
    
    /**
   * Đặt tọa độ x và y vào một vị trí cho trước.
     * Trong trường hợp này, tọa độ x và y sẽ có cùng giá trị.
     * @param pos vị trí trong khung, tính bằng pixel
     */
    public void setCoordinates(int pos){
        if(this.pIndex<2){
            this.coordinates[0]=TILE_SIZE*15-DICE_SIZE-pos;}
        else{
            this.coordinates[0]=pos;}
        if(this.pIndex%3==0){
            this.coordinates[1]=pos;}
        else{
            this.coordinates[1]=TILE_SIZE*15-DICE_SIZE-pos;}
    }

    /**
* Chọn một hình ảnh mới để đại diện cho xúc xắc với mỗi
lần đánh dấu của bộ đếm thời gian điều khiển hoạt ảnh của xúc xắc từ GUI.
     */
    public void animateDice() {
        pos += vel*tickCounter + ACC*tickCounter*tickCounter/2;
        if(pos<(TILE_SIZE*15-DICE_SIZE)/2){
            if(this.pIndex%3==0)
                diceImg = diceAnimation[tickCounter%diceAnimation.length];
            else
                diceImg = diceAnimation[diceAnimation.length-1-(tickCounter%diceAnimation.length)];
            tickCounter++;
            vel += ACC;}
        else{
            diceImg = dice[result-1];
            pos=(TILE_SIZE*15-DICE_SIZE)/2;}
        setCoordinates(pos);
        //System.out.println("dice pos "+pos);
    }
}
    
